module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Crea un query o mutation',
    prompts: [
      {
        type: 'list',
        name: 'queryType',
        message: `Recuerda que para generar un query o mutacion necesitas:
        -GQL Entity de entrada (si aplica)
        -Yup valdiations de entrada (si aplica)
        -Tipado de entidad de entrada (si aplica)
        -Mongoose schema de entidad a manipular
        -Tipado de entidad a manipular
        -GQL Entity de respuesta
        -Rutas (PATHS) de donde crear tu query/ mutacion y los archivos de donde provienen las definiciones anteriores:
        \nQue quieres generar?`,
        choices: [
          {
            name: 'Query',
            value: 'queries',
          },
          {
            name: 'Mutacion',
            value: 'mutations',
          },
        ],
      },
      {
        type: 'input',
        name: 'queryName',
        message: 'Nombre de tu query/mutacion, ejemplos: (createOne, makePurchase, getAllUsers)',
      },
      {
        type: 'input',
        name: 'queryPath',
        message:
          'PATH desde el rootQuery/rootMutation tu query/mutacion, ejemplo: ("purchases/customProd/" \n-Finaliza en "/"\nNo incluyas el path desde "src"\n:',
      },
      {
        type: 'confirm',
        name: 'withAuth',
        message: 'Incluir AuthValidator?',
        default: true,
      },
      {
        type: 'input',
        name: 'entityType',
        message: 'Nombre de la gql entidad de retorno: ',
      },
      {
        type: 'confirm',
        name: 'haveArgs',
        message: 'Tu query/mutación recibe argumentos?',
        default: false,
      },
      {
        type: 'input',
        name: 'inputParamsInterface',
        message: 'Nombre de la interfaz de inputParams: ',
        when: (data) => data.haveArgs,
      },
      {
        type: 'input',
        name: 'argGqlInputType',
        message: 'Nombre de la GQL_INPUT object del arg que esperas: ',
        when: (data) => data.haveArgs,
      },
      {
        type: 'input',
        name: 'yupSchema',
        message: 'Nombre del yup Schema con validaciones: ',
        when: (data) => data.haveArgs,
      },
      {
        type: 'input',
        name: 'resolverPath',
        message:
          'Path de la entidad del resolver, ejemplo: (src/entities/customProducts/ Finaliza en "/")',
      },
    ],
    /** @type {Actions} */
    actions: (data) => {
      const { haveArgs, withAuth } = data;
      if (haveArgs && withAuth) {
        return [
          {
            type: 'add',
            path: '../src/framework/graphql/schema/{{queryType}}/{{queryPath}}{{queryName}}/{{queryName}}.ts',
            templateFile: 'templates/args/Auth.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}resolvers/{{queryName}}Resolver.ts',
            templateFile: 'templates/args/resolver.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}logic/{{queryName}}Logic/{{queryName}}Logic.ts',
            templateFile: 'templates/args/logic.ts.hbs',
          },
        ];
      }
      if (haveArgs && !withAuth) {
        return [
          {
            type: 'add',
            path: '../src/framework/graphql/schema/{{queryType}}/{{queryPath}}{{queryName}}/{{queryName}}.ts',
            templateFile: 'templates/args/NoAuth.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}resolvers/{{queryName}}Resolver.ts',
            templateFile: 'templates/args/resolver.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}logic/{{queryName}}Logic/{{queryName}}Logic.ts',
            templateFile: 'templates/args/logic.ts.hbs',
          },
        ];
      }
      if (!haveArgs && withAuth) {
        return [
          {
            type: 'add',
            path: '../src/framework/graphql/schema/{{queryType}}/{{queryPath}}{{queryName}}/{{queryName}}.ts',
            templateFile: 'templates/args/Auth.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}resolvers/{{queryName}}Resolver.ts',
            templateFile: 'templates/args/resolver.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}logic/{{queryName}}Logic/{{queryName}}Logic.ts',
            templateFile: 'templates/args/logic.ts.hbs',
          },
        ];
      }
      if (!haveArgs && !withAuth) {
        return [
          {
            type: 'add',
            path: '../src/framework/graphql/schema/{{queryType}}/{{queryPath}}{{queryName}}/{{queryName}}.ts',
            templateFile: 'templates/noArgs/NoAuth.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}resolvers/{{queryName}}Resolver.ts',
            templateFile: 'templates/noArgs/resolver.ts.hbs',
          },
          {
            type: 'add',
            path: '../{{resolverPath}}logic/{{queryName}}Logic/{{queryName}}Logic.ts',
            templateFile: 'templates/noArgs/logic.ts.hbs',
          },
        ];
      }
      throw new Error('Caso no validado, reivisar plopfile.js');
    },
  });
};
/**
 * @typedef {(data: {
 * queryType: string;
 * queryName: string;
 * queryPath: string;
 * withAuth: boolean;
 * entityType: string;
 * haveArgs: boolean;
 * inputParamsInterface: string;
 * inputParamsInterfacePath: string;
 * argGqlInputType: string;
 * yupSchema: string;
 * resolverPath: string;
 * queryName: string;
 * })=>{[s: string]: unknown}[]} Actions
 */
