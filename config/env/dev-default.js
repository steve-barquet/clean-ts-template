const {} = require('./secrets');

const { env } = process;

module.exports = new (function () {
  // ------ General App config ------
  this.APP_PORT = 4000;
  this.NODE_ENV = env?.NODE_ENV;
  /** Activa/Desactiva la validación de auth en endpoint/query */
  this.AUTH_ENABLED = 'false';
  this.DOMAIN = 'localhost';
  this.CLIENT_URL = 'http://localhost:3000';
  this.BACK_END_URL = `http://${this.DOMAIN}:${this.APP_PORT}`;
})();
