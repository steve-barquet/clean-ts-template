const { env } = process;
const {} = require('./secrets');

module.exports = new (function () {
  // ------ General App config ------
  this.APP_PORT = 4000;
  this.NODE_ENV = env?.NODE_ENV;
  this.DOMAIN = 'api.forgemytech.com';
  /** Activa/Desactiva la validación de auth en endpoint/query */
  this.AUTH_ENABLED = 'true';
  /** Nota: HTTPS en la url probablemente */
  this.CLIENT_URL = 'https://forgemytech.com';
  /** Nota: HTTPS en la url probablemente */
  this.BACK_END_URL = `https://${this.DOMAIN}`;
})();
