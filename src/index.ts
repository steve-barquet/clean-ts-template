import { bigTestCode } from 'src/sub1/sub2/bigTestCode';

function main() {
  console.log('---------- Cool Programing ----------\n');
  smallTestCode();
  bigTestCode();
}

main();

function smallTestCode() {
  console.log('Do some small stuff :D');
}
